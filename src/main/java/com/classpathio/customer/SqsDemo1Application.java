package com.classpathio.customer;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDynamoDBRepositories(basePackages = "com.classpathio.customer.repository")
public class SqsDemo1Application {

	public static void main(String[] args) {
		SpringApplication.run(SqsDemo1Application.class, args);
	}

}
