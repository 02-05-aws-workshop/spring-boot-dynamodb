package com.classpathio.customer.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.classpathio.customer.model.Product;
import com.classpathio.customer.repository.ProductRepository;

@Component
public class DynamoDBClient implements CommandLineRunner {
	
	@Autowired
	private AmazonDynamoDB amazonDynamoDB;
	
	private ProductRepository productRepository;
	
	

	@Override
	public void run(String... args) throws Exception {
		//insert the products to the table
		
		Product product = new Product();
		product.setName("I-Phone");
		product.setPrice(74000);
		
		this.productRepository.save(product);
		
		
	}

}
