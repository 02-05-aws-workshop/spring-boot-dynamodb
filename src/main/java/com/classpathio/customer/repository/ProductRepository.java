package com.classpathio.customer.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.classpathio.customer.model.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, String> {

}

